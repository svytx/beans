#!/bin/python

# NOTE: terrible, horrible, no good, very bad algorithm

I = 'private.env'
O = I+'.sample'

src = open(I, 'r').readlines()
dst = src

for k, v in enumerate(src):
    if v == '# defaults\n': break

    x = v.split('=', 1)
    if len(x) == 1: continue
    if x[0].find('#') != -1: continue

    v = x[0]+'=[rainbow]\n'

    dst[k] = v

dst = ''.join(dst)
open(O, 'w').write(dst)
print(open(O, 'r').read())
