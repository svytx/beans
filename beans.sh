#!/bin/sh

DIR="$(realpath "$(CDPATH= cd -- "$(dirname -- $(readlink -f "$0"))" && pwd -P)")"

export DOTDROP_AUTOUPDATE="no"

. "$DIR/colors.env"
. "$DIR/private.env"

SUPROFILE="su_$PROFILE"

dotdrop() {
    "$DIR/dotdrop.sh" -p $PROFILE "$@"
}

sudotdrop() {
    sudo "$DIR/dotdrop.sh" -p $SUPROFILE "$@"
}

dotsync() {
    echo "stow: all:"
    stow -v -d beans -t ~ -S home

    #echo "ansible: beans,private: all"
    #ansible-playbook site.yml -t beans,private --vault-password-file=x/pw

    # TODO: "ansible: beans: home, home-desktop"

    echo "stow: desktop:"
    [ -z "$IS_DESKTOP" ] || stow -v -d beans -t ~ -S home-desktop

    echo "profile: $PROFILE:"
    dotdrop install

    if [ "$PROFILE" = "office" ]; then
        echo -e "\nprofile: $SUPROFILE:"
        sudotdrop install
    fi
}

dotsync
