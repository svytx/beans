#!/bin/bash

usg() {
    echo -e "Usage: $( basename $0 ) cmd [ args ]" 1>&2
    echo -e "\\tcmd              execute command with optional arguments"

    exit 1
}

[ $# -eq 0 ] && usg

if ! adb get-state 1>/dev/null 2>&1; then
    echo "Error: no devices/emulators found. Please connect a device."
    exit
fi

adb shell "su -c "/data/home/bin/termux.sh "$@"""

case $1 in
    sshd)
        adb forward tcp:8022 tcp:8022
        ;;
esac
