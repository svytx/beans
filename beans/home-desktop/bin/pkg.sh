#!/bin/bash

clean_world() {
    while read i ; do
        if [[ -n "$(qdepends -Q $i)" ]]; then
            echo '' ; echo 'checking '$i ;
            if [[ -n "$(emerge -p --quiet --depclean $i)" ]]; then
                echo $i' needs to stay in @world'
            else
                echo $i' can be deselected'
                echo $i >> /tmp/deselect
            fi
        fi
    done < /var/lib/portage/world

    echo 'Entries collected in /tmp/deselect can be cross-checked using the --depclean option: '
    echo 'emerge -pv --depclean $(< /tmp/deselect)'
    echo 'The emerge --deselect option will remove these entries without unmerging them.'
    echo 'sudo emerge --ask --deselect $(< /tmp/deselect)'

    # https://wiki.gentoo.org/wiki/Selected_set_(Portage)
}

sync() {
    emerge --sync --quiet
}

upd() {
    # emerge -auDN --quiet-build @world
    emerge -auDU --keep-going --with-bdeps=y --quiet-build @world
}

add() {
    emerge -a --quiet-build "$1"
}

add1() {
    emerge -a1 --quiet-build "$1"
}

remove() {
    emerge -cav "$1"
}

show_use() {
    equery uses "$1"
}

usg() {
    echo -e "Usage: $( basename $0 ) [ sync | upd | clean-world | remove PKG | add PKG | add1 PKG | show-use PKG ]"
    exit 1
}

case "$1" in
    add) add "$2" ;;
    add1) add1 "$2" ;;
    remove) remove "$2" ;;
    sync) sync ;;
    upd) upd ;;
    clean-world) clean_world ;;
    show-use) show_use "$2" ;;
    *) usg ;;
esac

# tail -f /var/tmp/portage/dev-qt/qtwebengine-5.12.3/temp/build.log
