#!/bin/bash

export DEV=/dev/sr0

usg() {
    echo -e Usage: $( basename $0 ) '[cd | dvd | verify] <ISO>' 1>&2
    echo -e "  dvd          media type: dvd"
    echo -e "  cd           media type: cd"
    echo -e "  verify       veridy data integrity"
    exit 1
}

verify() {
    echo 'Verifying checksum...'

    chksum1=($(md5sum $1))
    echo md5 1: $chksum1

    blocks=$(expr $(du -b $1 | awk '{print $1}') / 2048)
    #chksum2=($(dd if=$DEV bs=2048 count=$blocks 2>/dev/null | md5sum))
    chksum2=($(dd if=$DEV bs=2048 count=$blocks | md5sum))
    echo md5 2: $chksum2
}

cdr() {
    [ $(mount | grep -c $DEV) == 1 ] && sudo umount $DEV
    wodim -v -sao dev=$DEV $1
}

dvd() {
    [ $(mount | grep -c $DEV) == 1 ] && sudo umount $DEV

    growisofs -speed=1 -dvd-compat -Z $DEV=$1
}

[ $# -ne 2 ] && usg

case $1 in
    cd)
        cdr $2
        ;;
    dvd)
        dvd $2
        ;;
    verify)
        verify $2
        ;;
    *)
        usg
esac
