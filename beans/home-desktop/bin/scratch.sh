#!/bin/bash

xwininfo -name scratch
if [ "$?" = 0 ]; then
    i3-msg \[title="^scratch$"\] scratchpad show
else
    cd ~/scratch
    st -g '=100x30' -T scratch &

    for ((n = 1; n < 20; ++n)); do
        xwininfo -name scratch
        if [ "$?" = 0 ]; then
            i3-msg \[title="^scratch$"\] scratchpad show
            break
        fi
    done
fi
