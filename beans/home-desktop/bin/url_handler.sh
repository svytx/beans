#! /bin/bash

BRWS_TUI=w3m
MAILR=neomutt
XTERM=st

url_vid="^https://\(www\.youtube\.com\)"

vid="mkv mp4 webm"
img="jpg jpeg png img"

if [ "$2" != "" ]; then
    ext=$2
else
    ext="${1##*.}"
    ext="${ext,,}" # to lowercase
fi

# copy url to clipboard
echo "$1" | xclip -selection clipboard

if [ "$ext" = "gif" ]; then
    curl -o /tmp/sxivimg -s "$1"; sxiv -g 640x480 -N forcefloat -a -q /tmp/sxivimg >/dev/null & disown #; rm /tmp/sxivimg
elif echo "$img" | grep -wi "$ext" > /dev/null; then
    feh -x --title "forcefloat" -g 640x480 "$1" >/dev/null & disown
elif echo "$vid" | grep -wi "$ext" > /dev/null; then
    mpv -quiet "$1" > /dev/null & disown
elif echo "$1" | grep "$url_vid" > /dev/null; then
    mpv -quiet "$1" > /dev/null & disown
elif [ "$ext" = "pdf" ]; then
    zathura "$1" > /dev/null & disown
elif echo "$1" | grep "^ftp://" > /dev/null; then
    $XTERM $BRWS_TUI "$1" >/dev/null & disown
elif echo "$1" | grep "^mailto:" > /dev/null; then
    $XTERM $MAILR "$1" >/dev/null & disown
else
    #$BROWSER "$1" > /dev/null & disown
    #nohup $BROWSER "$1" > /dev/null 2>&1 &
    setsid $BROWSER "$1" > /dev/null 2>&1 &
fi
