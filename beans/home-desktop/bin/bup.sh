#!/bin/bash

usg() {
    echo -e Usage: $( basename $0 ) '<bup directory>'
    exit 1
}

[ $# -eq 0 ] && usg

export BUP_DIR="$1"

bup "${@:2}"
