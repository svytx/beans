#!/bin/bash

m=( $(xrandr | grep " connected" | sed -e "s/\([A-Z0-9]\+\) connected.*/\1/") )
dir=( left right )
inv=$(( $(( $1 + 1 )) % 2 ))

xrandr --listmonitors | grep "${m[$1]}" > /dev/null
if [ "$?" -eq "0" ]; then
    xrandr --output ${m[$1]} --off
else
    xrandr --output ${m[$1]} --auto --${dir[$1]}-of ${m[$inv]}
fi
