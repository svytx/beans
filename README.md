# beans

## Prerequisites

git, ansible, stow, python, pip.

## Install:

- Clone
  ```sh
  git clone --recurse-submodules -j10 "git@gitlab.com:svytx/beans.git" "$HOME"
  cd "$HOME/beans"

  # TODO: use pipenv with $PIPENV_VENV_IN_PROJECT
  pip3 install -r dotdrop/requirements.txt --user
  ./dotdrop/bootstrap.sh
  ```
- Create `private.env` for one of the profiles (edit `private.env.sample`).
- TODO: ansible setup
- ~~Run scripts in depinstall directory.~~

### Available profiles:

* home
* office
* vlap0

## Using:

Run `beans.sh` to sync the beans.

## stow

- all profiles: `beans/home` to `$HOME`
- desktops: `beans/home-desktop` to `$HOME`

## dotdrop

### Updating

```sh
git submodule update --init --recursive
git submodule update --remote dotdrop

// TODO: use pipenv
sudo pip3 install -r dotdrop/requirements.txt
```

## ansible

Everything:
```sh
ansible-playbook site.yml
```

Use `--check` to "dry run".

Use tags (use with `-t` [tags]) for selective tasks.

Available tags:
- local-setup
- beans
- private
- install

For semi-private (aka *I don't care if leaks*) encrypted beans,
create a file with a password in `x/pw` and run:
```sh
ansible-playbook site.yml -t beans,private --vault-password-file=x/pw
```

## TODO

* This is a very incomplete setup. Use virtual machine to test environment /
install from scratch.

* Incorporate `pass` for secrets.

* How to sync *foil hat paranoia sensitivity* files like `pass` db?
