#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR_TMP_ADAPTA="/tmp/adapta"
VER_ADAPTA="3.93.0.237"
DIR_TMP_PAPER="/tmp/paper"
VER_PAPER="1.4.0"

is_installed() {
    if [ -f "$1/ver" ]; then
        [ "$( cat "$1/ver" )" = "$2" ] && return 0
        rm -rf "$1"
        return 1
    fi
    return 1
}

adapta() {
    dir_dst="$HOME/.themes/Adapta-Nokto-Eta"

    is_installed "$dir_dst" "$VER_ADAPTA" && return
    echo "Installing adapta"

    Z="$VER_ADAPTA.tar.gz"

    mkdir -p "$DIR_TMP_ADAPTA"
    mkdir -p "$HOME/.themes"
    cd "$DIR_TMP_ADAPTA"
    wget -q "https://github.com/adapta-project/adapta-gtk-theme/archive/$Z"
    tar xf "$Z"
    cd "adapta-gtk-theme-$VER_ADAPTA"
    ./autogen.sh --prefix="$DIR_TMP_ADAPTA" \
        --enable-parallel \
        --disable-gnome \
        --disable-cinnamon \
        --disable-flashback \
        --disable-xfce \
        --disable-mate \
        --disable-openbox
    make
    make install
    mv "$DIR_TMP_ADAPTA/share/themes/Adapta-Nokto-Eta" "$HOME/.themes"
    cd
    rm -rf "$DIR_TMP_ADAPTA"
    echo "$VER_ADAPTA" > "$dir_dst/ver"
}

paper() {
    dir_dst="$HOME/.local/share/icons/Paper"

    is_installed "$dir_dst" "$VER_PAPER" && return
    echo "Installing paper"

    Z="v$VER_PAPER.tar.gz"

    mkdir -p "$DIR_TMP_PAPER"
    mkdir -p "$HOME/.local"
    cd "$DIR_TMP_PAPER"
    wget -q "https://github.com/snwh/paper-icon-theme/archive/$Z"
    tar xf "$Z"
    cd "paper-icon-theme-$VER_PAPER"
    ./autogen.sh
    DESTDIR="$DIR_TMP_PAPER" make install
    rsync -aAX "$DIR_TMP_PAPER/usr/" "$HOME/.local"
    cd
    rm -rf "$DIR_TMP_PAPER"
    echo "$VER_PAPER" > "$dir_dst/ver"
}

# TODO:
# * check dependencies

paper
adapta
